# TTzinho

Um pequeno robô desenvolvido pelo Principia - Robôs na Escola para o ensino de robótica.

Ele funciona no modo de controle de remoto pelo celular e no modo autonomo.

Baixe o aplicativo [nesse link](https://github.com/simoesusp/Android_Remote_Control/blob/master/controle-robo-wifi-config.apk).

Depois de baixar o aplicativo, conecte o celular na rede "ESP32 ACESS POINT".
A senha é "your-password".

Quando abrir o aplicativo escolha o ip 192.168.4.1

Se estiver no modo automático basta apertar o "Para frente" uma vez.
Para fazer ele virar posicione a mão em frente ao sonar.

Se estiver no modo de controle remoto é só pilotar.
Aperte uma vez para frente para ele comecar a andar e segure o botão de esquerda e direita para ele virar.